//
//  RecordingTableViewCell.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import UIKit

class RecordingTableViewCell: UITableViewCell {
    // MARK - Properties
    
    // MARK: - IBOutlets
    // Title label
    @IBOutlet weak var titleLabel: UILabel!
    // Date label
    @IBOutlet weak var dateLabel: UILabel!
    // Play button
    @IBOutlet weak var playButton: UIButton!
    
    // Play button closure
    var playButtonClosure: (() -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        playButton.addTarget(self, action: #selector(playButtonWasTapped), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Play button action
    @objc func playButtonWasTapped() {
        playButtonClosure?()
    }
}
