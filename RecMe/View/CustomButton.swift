//
//  RoundButton.swift
//  Swipe Actions
//
//  Created by Marco Margarucci on 04/10/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    //Corner radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    // Border width
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    // Border color
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    // Shadow color
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    // Shadow offset
    @IBInspectable var shadowOffset = CGSize(width: 0, height: 0) {
        didSet {
            self.layer.shadowOffset = CGSize(width: 5, height: 5)
        }
    }
    
    // Shadow radius
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    // Shadow opacity
    @IBInspectable var shadowOpacity: Float = 0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
        
    // Attributed title
//    @IBInspectable var setAttributedTitle: String = "" {
//        didSet {
//            let attribute = NSMutableAttributedString(string: "\(UI.Button.NoAccount.firstTitle)\(UI.Button.NoAccount.secondTitle)")
//            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UI.Button.NoAccount.firstTitleColor, range: NSRange(location: 0, length: UI.Button.NoAccount.firstTitle.count))
//            attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UI.Button.NoAccount.secondTitleColor, range: NSRange(location: UI.Button.NoAccount.firstTitle.count, length: UI.Button.NoAccount.secondTitle.count))
//            self.setAttributedTitle(attribute, for: .normal)
//        }
//    }
}
