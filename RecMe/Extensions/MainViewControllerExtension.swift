//
//  MainViewControllerExtension.swift
//  RecMe
//
//  Created by Marco Margarucci on 21/09/21.
//

import UIKit
import AVFoundation

// Handle recoding button
extension MainViewController {
    
    func handleFirstRecordingButton() {
        if CoreDataService.shared.numberOfItems() <= 0 {
            addFirstRecordingButton.addTarget(self, action: #selector(addFirstRecordingButtonAction), for: .touchUpInside)
            addFirstRecordingButton.alpha = .zero
            addFirstRecordingButton.transform = CGAffineTransform(translationX: .zero, y: 60)
            dataTableView.isHidden = true
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.20, options: .curveEaseOut, animations: {
                self.addFirstRecordingButton.isHidden = false
                self.addFirstRecordingButton.transform = CGAffineTransform(translationX: .zero, y: .zero)
                self.addFirstRecordingButton.alpha = 1.0
            }, completion: nil)
        } else {
            addFirstRecordingButton.isHidden = true
            dataTableView.isHidden = false
        }
    }
    
    // Add first recording button action
    @objc fileprivate func addFirstRecordingButtonAction() {
        performSegue(withIdentifier: "RecordedView", sender: nil)
    }
}

// Table view delegate, table view data source
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        handleFirstRecordingButton()
        return CoreDataService.shared.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecordingTableViewCell
        let currentObject = CoreDataService.shared.recordedData[indexPath.row]
        cell.titleLabel.text = currentObject.title
        cell.dateLabel.text = dateToString(currentObject.date!)
        cell.playButton.tag = indexPath.row
        cell.playButtonClosure = { [unowned self] in
            do {
                self.player = try AVAudioPlayer(data: currentObject.audioData!)
                self.player?.play()
                debugPrint("Playing...")
            } catch let error {
                debugPrint("Error playing the record: \(error.localizedDescription)")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let editAction = UIContextualAction(style: .normal, title: "Edit") { _, _, _ in
            self.presentAlert(row: indexPath.row)
        }
        editAction.backgroundColor = UIColor(named: "recordButton")
        editAction.image = UIImage(systemName: "rectangle.and.pencil.and.ellipsis")
        let swipeAction = UISwipeActionsConfiguration(actions: [editAction])
        return swipeAction
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { _, _, _ in
            let deletingRecording = CoreDataService.shared.recordedData[indexPath.row]
            CoreDataService.shared.appViewContext().delete(deletingRecording)
            CoreDataService.shared.recordedData.remove(at: indexPath.row)
            self.dataTableView.deleteRows(at: [indexPath], with: .automatic)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
                self.dataTableView.reloadData()
            }
            CoreDataService.shared.currentCoreData(state: .save)
        }
        deleteAction.backgroundColor = UIColor(named: "recordButton")
        deleteAction.image = UIImage(systemName: "xmark.bin")
        let swipeAction = UISwipeActionsConfiguration(actions: [deleteAction])
        return swipeAction
    }
}

// Update table view
extension MainViewController: UpdateTableView {
    func updateTableView() {
        CoreDataService.shared.currentCoreData(state: .load)
        dataTableView.reloadData()
    }
}
