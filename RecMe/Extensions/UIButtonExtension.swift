//
//  UIButtonExtension.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import UIKit

extension UIButton {
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    func shouldSetupSFSymbols(imageName: String, fontSize: CGFloat) {
        guard let image = UIImage(systemName: imageName) else { return }
        let symbolConfiguration = UIImage.SymbolConfiguration(pointSize: fontSize, weight: .heavy)
        setPreferredSymbolConfiguration(symbolConfiguration, forImageIn: .normal)
        setImage(image, for: .normal)
    }
}
