//
//  RecorderViewController.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import UIKit
import AVFoundation

class RecorderViewController: UIViewController {
    // MARK: - IBOutlets
    // Record button
    @IBOutlet weak var recordButton: UIButton!
    // Recorder label
    @IBOutlet weak var recorderLabel: UILabel!
    
    // MARK: - Properties
    
    // Recording state
    private var recordingState: RecordingState = .start
    // Used to check if the stop button was tapped
    var stopButtonWasTapped: Bool = false
    // Delegate
    var delegate: UpdateTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startRecording()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if recordingState == .stop && !stopButtonWasTapped {
            stopAndSaveRecording()
        }
    }
    
    // MARK: - Functions
    
    // Start recording
    fileprivate func startRecording() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission { _ in
                
            }
        case .denied:
            recorderLabel.text = "Microphone access was denied"
        case .granted:
            break
        default:
            break
        }
        
        _ = AudioRecorder.shared.shouldManageRecord(.started)
        recordingState = .stop
        recordButton.setImage(UIImage(named: "stop"), for: .normal)
        recorderLabel.text = "recording..."
    }
    
    // Stop and save recording
    fileprivate func stopAndSaveRecording() {
        let recordedObject = Recorded(context: CoreDataService.shared.appViewContext())
        recordedObject.title = "Recording \(CoreDataService.shared.numberOfItems())"
        recordedObject.date = Date()
        recordedObject.audioData = AudioRecorder.shared.shouldManageRecord(.ended)
        // Persist recorded data
        CoreDataService.shared.currentCoreData(state: .save)
        // Remove recorded data from memory
        AudioRecorder.shared.shouldDeinitialize()
        delegate?.updateTableView()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func recordButtonWasTapped(_ sender: Any) {
        stopButtonWasTapped = true
        stopAndSaveRecording()
    }
}

// Recording state
enum RecordingState {
    case start, stop
}
