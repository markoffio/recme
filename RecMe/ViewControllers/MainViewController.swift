//
//  MainViewController.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController {
    // MARK: - Properties
    
    // MARK: - IBOutlets
    // Add recording button
    @IBOutlet weak var addRecordingButton: UIBarButtonItem!
    // Data table view
    @IBOutlet weak var dataTableView: UITableView!
    // Add first recording button
    @IBOutlet weak var addFirstRecordingButton: CustomButton!
    
    // Player
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUserInterface()
    }

    // Setup user interface
    func setupUserInterface() {
        //addRecordingButton.tintColor = UIColor(named: "buttonBackground")
        CoreDataService.shared.currentCoreData(state: .load)
        // Setup table view
        setupTableView()
    }
    
    // MARK: - Functions
    
    // Setup table view
    fileprivate func setupTableView() {
        UITableView.appearance().backgroundColor = .clear
        UITableViewCell.appearance().backgroundColor = .clear
        dataTableView.delegate = self
        dataTableView.dataSource = self
        dataTableView.register(UINib(nibName: "RecordingTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
    }
    
    // Present alert to edit the recording
    func presentAlert(row: Int) {
        let alertView = UIAlertController(title: "Edit title", message: "Change recording title", preferredStyle: .alert)
        let currentRecording = CoreDataService.shared.recordedData[row]
        alertView.addTextField { textField in
            textField.text = currentRecording.title
        }
        let action = UIAlertAction(title: "Done", style: .default) { _ in
            if let textField = alertView.textFields?.first {
                currentRecording.title = textField.text
                CoreDataService.shared.currentCoreData(state: .save)
                self.dataTableView.reloadData()
            }
        }
        alertView.addAction(action)
        present(alertView, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RecordedView" {
            let destination = segue.destination as! RecorderViewController
            destination.delegate = self
        }
    }
}

