//
//  CoreDataService.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import UIKit
import CoreData

// CoreDataState enumeration is used to managed audio recording tasks
enum CoreDataState {
    case save, load
}

class CoreDataService {
    // MARK - Properties
    
    // Shared instance
    static let shared = CoreDataService()
    // Recorded data
    lazy var recordedData: [Recorded] = []
    
    init() {}
    
    // Return the number of items
    func numberOfItems() -> Int {
        recordedData.count
    }
    
    // Return an NSManagedObjectContext
    func appViewContext() -> NSManagedObjectContext {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    // Load records from CoreData in ascending order
    private func shouldLoadCoreData() {
        let fetchRequest: NSFetchRequest<Recorded> = Recorded.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Recorded.date), ascending: false)]
        do {
            recordedData = try appViewContext().fetch(fetchRequest)
        } catch let error {
            debugPrint("Error loading data from CoreData: \(error.localizedDescription)")
        }
    }
    
    // Save records to CoreData
    private func shouldSaveCoreDataObject() {
        do {
            try appViewContext().save()
        } catch let error {
            debugPrint("Error saving core data object: \(error.localizedDescription)")
        }
    }
    
    func currentCoreData(state: CoreDataState) {
        switch state {
        case .load:
            shouldLoadCoreData()
        case .save:
            shouldSaveCoreDataObject()
        }
    }
}
