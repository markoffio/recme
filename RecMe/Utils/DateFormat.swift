//
//  DateFormat.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import Foundation
import UIKit

// Convert date to string
func dateToString(_ date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium
    dateFormatter.timeStyle = .short
    return dateFormatter.string(from: date)
}
