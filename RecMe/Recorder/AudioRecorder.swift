//
//  AudioRecorder.swift
//  RecMe
//
//  Created by Marco Margarucci on 15/09/21.
//

import UIKit
import AVFoundation

class AudioRecorder {
    // MARK: - Properties
    
    //
    private struct Static {
       static var shared: AudioRecorder?
    }
    
    class var shared: AudioRecorder {
        if Static.shared == nil {
            Static.shared = AudioRecorder()
        }
        return Static.shared!
    }
    
    private var audioRecorder: AVAudioRecorder!
    
    // Recording settings
    var recordingSettings: [String: Int] = [AVSampleRateKey: 12000, AVFormatIDKey: Int(kAudioFormatMPEG4AAC), AVNumberOfChannelsKey: 2, AVEncoderAudioQualityKey: AVAudioQuality.medium.rawValue]
    
    private var recordingSession: AVAudioSession?
    
    // Recorded audio track
    private var recordedTrack: Data?
    
    init() {
        shouldSetupAudioRecorder()
    }
    
    // Deinitializer
    func shouldDeinitialize() {
        Static.shared = nil
    }
    
    private func shouldSetupAudioRecorder() {
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession!.setCategory(.playAndRecord, mode: .spokenAudio, options: .defaultToSpeaker)
            // Inizialize audio recorder
            try audioRecorder = AVAudioRecorder(url: recordingURL(), settings: recordingSettings)
        } catch let error {
            debugPrint("Error: \(error.localizedDescription)")
        }
    }
    
    private func recordingURL() -> URL {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        documentDirectory.removeAllCachedResourceValues()
        let recordingURL = documentDirectory.appendingPathComponent("audio")
        return recordingURL!
    }
    
    // Start recording the audio track
    private func startRecording() {
        do {
            if let session = recordingSession {
                try session.setActive(true)
                audioRecorder.record()
            }
        } catch let error {
            debugPrint("Error staring recording the audio track: \(error.localizedDescription)")
        }
    }
    
    // Stop recording the audio track
    private func stopRecording() {
        audioRecorder.stop()
        do {
            if let session = recordingSession {
                try session.setActive(false)
            }
        } catch let error {
            debugPrint("Error stopping recording the audio track: \(error.localizedDescription)")
        }
        // Audio track
        var audioTrack: Data?
        do {
            audioTrack = try Data(contentsOf: audioRecorder.url)
        } catch let error {
            debugPrint("Error: \(error.localizedDescription)")
        }
        if let track = audioTrack {
            recordedTrack = track
        }
    }
    
    // Manage record state
    func shouldManageRecord(_ audioRecorderState: AudioRecorderState) -> Data? {
        switch audioRecorderState {
        case .started:
            startRecording()
            return nil
        case .ended:
            stopRecording()
            return recordedTrack
        }
    }
}

enum AudioRecorderState {
    case started, ended
}
