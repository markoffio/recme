//
//  UpdateTableView.swift
//  RecMe
//
//  Created by Marco Margarucci on 21/09/21.
//

import Foundation

protocol UpdateTableView {
    func updateTableView()
}
